import { IsNotEmpty, MinLength, Matches } from 'class-validator';
import { IsEmpty } from 'class-validator/types/decorator/decorators';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;
  @IsNotEmpty()
  @MinLength(5)
  name: string;
  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  password: string;
}
